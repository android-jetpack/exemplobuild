package br.gov.go.aparecida.exemplobuildauto

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        txtVersion.text = "Version Code: ${BuildConfig.VERSION_CODE}" +
                          "\nVersion Name: ${BuildConfig.VERSION_NAME}"+
                          "\nNome do App: ${getString(R.string.APP_NAME)}"+
                          "\nURL: ${BuildConfig.REST_API_ENDPOINT}"
    }
}
